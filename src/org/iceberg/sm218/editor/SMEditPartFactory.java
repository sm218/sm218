package org.iceberg.sm218.editor;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.iceberg.sm218.editor.editpart.SMDiagramEditPart;
import org.iceberg.sm218.editor.editpart.SMEditPartActor;
import org.iceberg.sm218.editor.editpart.SMEditPartFrameSource;
import org.iceberg.sm218.editor.editpart.SMEditPartNeuron;
import org.iceberg.sm218.editor.editpart.SMEditPartTonic;
import org.iceberg.sm218.editor.editpart.SMEditPartWire;
import org.iceberg.sm218.editor.model.SMActor;
import org.iceberg.sm218.editor.model.SMConnection;
import org.iceberg.sm218.editor.model.SMDiagram;
import org.iceberg.sm218.editor.model.SMFrameSource;
import org.iceberg.sm218.editor.model.SMNeuron;
import org.iceberg.sm218.editor.model.SMTonic;

public class SMEditPartFactory implements EditPartFactory {

	@Override
	public EditPart createEditPart(EditPart context, Object model) {
		EditPart editPart = null;

		if(model instanceof SMDiagram) { 
			editPart = new SMDiagramEditPart(); 
		} else if(model instanceof SMNeuron) {
			editPart = new SMEditPartNeuron();
		} else if(model instanceof SMTonic) {
			editPart = new SMEditPartTonic();
		} else if(model instanceof SMConnection) {
			editPart = new SMEditPartWire();
		} else if(model instanceof SMFrameSource) {
			editPart = new SMEditPartFrameSource();
		} else if(model instanceof SMActor) {
			editPart = new SMEditPartActor();
		}

		if (editPart != null) {
			editPart.setModel(model);
		}

		return editPart;
	}

}
