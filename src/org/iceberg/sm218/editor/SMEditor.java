package org.iceberg.sm218.editor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.EventObject;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.iceberg.sm218.editor.editpart.SMDiagramEditPart;
import org.iceberg.sm218.editor.model.ProcessController;
import org.iceberg.sm218.editor.model.SMDiagram;

public class SMEditor extends GraphicalEditorWithFlyoutPalette {

	protected SMDiagram diagram = new SMDiagram();
	protected boolean editorSaving = false;
	protected ScalableFreeformRootEditPart root = null;
	
	public SMEditor() {
		setEditDomain(new DefaultEditDomain(this));
	}

	public synchronized SMDiagram getDiagram() {
		return this.diagram;
	}
	
	public synchronized SMDiagramEditPart getDiagramEditPart() {
		Iterator<?> childs = root.getChildren().iterator();
		while(childs.hasNext()) {
			Object current = childs.next();
			if(current instanceof SMDiagramEditPart)
				return (SMDiagramEditPart)current;
		}
		return null;
	}

	protected void setDiagram(SMDiagram diagram) {
		this.diagram = diagram;
	}
	
	protected void writeToOutputStream(OutputStream os) throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(os);
		out.writeObject(getDiagram());
		out.close();
	}
	
	public void setConnectionRouter(int type) {
		getDiagramEditPart().setConnectionRouter(type);
	}
	
	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		GraphicalViewer viewer = getGraphicalViewer();

		root = new ScalableFreeformRootEditPart();
		viewer.setRootEditPart(root);

		viewer.setEditPartFactory(new SMEditPartFactory());
	}
	
	@Override
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
		controller = new ProcessController(this);
		getGraphicalViewer().setContents(getDiagram());
	}
	
	@Override
	protected PaletteRoot getPaletteRoot() {
		return SMPalette.getSingleton().createPalette();
	}

	@Override
	public void commandStackChanged(EventObject event) {
		firePropertyChange(IEditorPart.PROP_DIRTY);
		super.commandStackChanged(event);
	}
	
	@Override
	protected void setInput(IEditorInput input) {
		super.setInput(input);
		IFile file = ((IFileEditorInput) input).getFile();
		try {
			InputStream is = file.getContents(false);
			ObjectInputStream ois = new ObjectInputStream(is);
			setDiagram((SMDiagram) ois.readObject());
			ois.close();
		} catch (Exception e) {
			// This is just an example. All exceptions caught here.
			//e.printStackTrace();
		}
			//  until editor not save all contents
		if (!editorSaving) {
			if (getGraphicalViewer() != null) {
				getGraphicalViewer().setContents(getDiagram());
			}
		}
	}
	
	@Override
	public void doSave(IProgressMonitor progressMonitor) {
		editorSaving = true;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			writeToOutputStream(baos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		IFile file = ((IFileEditorInput) getEditorInput()).getFile();
		try {
			file.setContents(new ByteArrayInputStream(baos.toByteArray()),
					true, false, progressMonitor);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getCommandStack().markSaveLocation();
		editorSaving = false;
	}
	
	private ProcessController controller;	
	
	public void startLong() {
		controller.setState(ProcessController.ACTIVE_STATE);
	}
	
	public void stopLong() {
		controller.setState(ProcessController.INACTIVE_STATE);
		//sstate =false;
	}
	
	@Override
	public void dispose() {
		stopLong();
		super.dispose();
	}

}
