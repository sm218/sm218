package org.iceberg.sm218.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.requests.SimpleFactory;
import org.iceberg.sm218.editor.model.SMActor;
import org.iceberg.sm218.editor.model.SMConnection;
import org.iceberg.sm218.editor.model.SMFrameSource;
import org.iceberg.sm218.editor.model.SMNeuron;
import org.iceberg.sm218.editor.model.SMTonic;

public class SMPalette {

	private static SMPalette instance = null;
	
	private static List<PaletteEntry> createCategories(PaletteRoot root){
		List<PaletteEntry> categories = new ArrayList<PaletteEntry>();
		
		categories.add(createControlGroup(root));
		categories.add(createWireControlGroup(root));
		categories.add(createComponentsDrawer(root));
	
		return categories;
	}
	
	static private PaletteContainer createWireControlGroup(PaletteRoot root){
		PaletteGroup controlGroup = new PaletteGroup("Wire control group");

		List<ToolEntry> entries = new ArrayList<ToolEntry>();

		ToolEntry tool = new ConnectionCreationToolEntry (
				"Connection",
				"Connection",
				new SimpleFactory(SMConnection.class),
				null,
				null);
		entries.add(tool);
		
		controlGroup.addAll(entries);
		return controlGroup;	
	}
	
	static private PaletteContainer createControlGroup(PaletteRoot root){
		PaletteGroup controlGroup = new PaletteGroup("Control group");

		List<ToolEntry> entries = new ArrayList<ToolEntry>();

		ToolEntry tool = new PanningSelectionToolEntry();
		entries.add(tool);
		

		root.setDefaultEntry(tool);
		
		controlGroup.addAll(entries);
		return controlGroup;
	}

	public PaletteRoot createPalette() {
		PaletteRoot palette = new PaletteRoot();
		palette.addAll(createCategories(palette));
		return palette;
	}
	
	public synchronized static SMPalette getSingleton() {
		if(instance == null) {
			instance = new SMPalette();
		}
		return instance;
	}

	static private PaletteDrawer createComponentsDrawer(PaletteRoot root) {
		PaletteDrawer drawer = new PaletteDrawer("Componnets", null);
		List<ToolEntry> entries = new ArrayList<ToolEntry>();
		
		CombinedTemplateCreationEntry combined = new CombinedTemplateCreationEntry (
				"Neuron",
				"Neuron I type",
				new SimpleFactory(SMNeuron.class),
				null,
				null);
		entries.add(combined);
		
		combined = new CombinedTemplateCreationEntry (
				"Sensor",
				"Binary tonic",
				new SimpleFactory(SMTonic.class),
				null,
				null);
		entries.add(combined);
		
		combined = new CombinedTemplateCreationEntry (
				"Frame source",
				"Source with space vehicle frame",
				new SimpleFactory(SMFrameSource.class),
				null,
				null);
		entries.add(combined);
		
		combined = new CombinedTemplateCreationEntry (
				"Actor",
				"System with knowledge base, actor and eval subsystem.",
				new SimpleFactory(SMActor.class),
				null,
				null);
		entries.add(combined);

		
		drawer.addAll(entries);
		return drawer;
	}
	
	
}
