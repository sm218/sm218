package org.iceberg.sm218.editor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

/**
 * 
 * Class for store images, icons and other sources,<br>
 * that can be used twice or more times.
 * 
 * @author Alexander Strakh
 *
 */
public class SourceHelper {
	
	protected static SourceHelper instance = null;
	
	protected static Map<String, Object> sources = getPreloadedSources();
	
	private static Map<String, Object> getPreloadedSources() {
		Map<String, Object> preloadedSources = new HashMap<String, Object>();
		return preloadedSources;
	}
	
	protected SourceHelper() {
	}
	

	/**
	 * Load from FS or store from map <br>
	 * (if it already loaded)<br>
	 * image by path <br>
	 * 
	 * @param id - full path to image
	 * @return Image 
	 */
	public Image getImage(String relPath) {
		Object object = sources.get(relPath);
		if(object == null) {
			ImageData imageData = null;
			InputStream in = null;
			try {
				URL url = SourceHelper.class.getResource(relPath);
				in = url.openStream();
				imageData = new ImageData(in);
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				if(in != null)
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
				
			object = new Image(null, imageData);
			sources.put(relPath, object);
		}
		return (Image)object;
	}	
	
	public static synchronized SourceHelper getSingleton() {
		if (instance == null) {
			instance = new SourceHelper();
		}
		return instance;
	}	
}
