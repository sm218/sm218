package org.iceberg.sm218.editor.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.iceberg.sm218.editor.SMEditor;

public abstract class AbstractAction implements IEditorActionDelegate {

	IEditorPart editor;
	
	public abstract void runAction(SMEditor editor);
	
	@Override
	public void run(IAction action) {
		if(editor instanceof SMEditor) {
			runAction((SMEditor)editor);
		}
	}
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		this.editor = targetEditor;
	}

}
