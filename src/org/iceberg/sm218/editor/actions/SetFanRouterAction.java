package org.iceberg.sm218.editor.actions;

import org.iceberg.sm218.editor.SMEditor;
import org.iceberg.sm218.editor.editpart.SMDiagramEditPart;

public class SetFanRouterAction extends AbstractAction {

	@Override
	public void runAction(SMEditor editor) {
		editor.setConnectionRouter(SMDiagramEditPart.CRT_FAN);
	}

}
