package org.iceberg.sm218.editor.actions;

import org.iceberg.sm218.editor.SMEditor;

public class StartLongAction extends AbstractAction {

	@Override
	public void runAction(SMEditor editor) {
		editor.startLong();
	}

}
