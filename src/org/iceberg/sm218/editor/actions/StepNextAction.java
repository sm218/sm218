package org.iceberg.sm218.editor.actions;

import org.iceberg.sm218.editor.SMEditor;
import org.iceberg.sm218.editor.actions.AbstractAction;

public class StepNextAction extends AbstractAction {
	
	@Override 
	public void runAction(SMEditor editor) {
		editor.getDiagramEditPart().nextTime();
		editor.getDiagramEditPart().getFigure().repaint();
	}
	

}
