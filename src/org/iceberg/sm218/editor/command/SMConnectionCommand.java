package org.iceberg.sm218.editor.command;

import org.eclipse.gef.commands.Command;
import org.iceberg.sm218.editor.editpart.SMEditPartNode;
import org.iceberg.sm218.editor.model.SMConnection;
import org.iceberg.sm218.editor.model.SMNode;

public class SMConnectionCommand extends Command {
	
	protected SMEditPartNode source;
	protected String sourceTerminal;
	protected SMEditPartNode target;
	protected String targetTerminal;
	protected SMConnection wire;

	
	public void setWire(SMConnection w) {
        wire = w;
	}


	public void setSource(SMEditPartNode newSource) {
        source = newSource;
	}
	
	public void setSourceTerminal(String newSourceTerminal) {
        sourceTerminal = newSourceTerminal;
	}


	public void setTarget(SMEditPartNode model) {
		target = model; 
	}


	public void setTargetTerminal(String mapConnectionAnchorToTerminal) {
		targetTerminal = mapConnectionAnchorToTerminal;
	}

	public void redo() { 
		execute(); 
	}
	
	public void execute() {

		if (source != null){
			wire.detachSource();
			source.refreshEPSourceConnections();
			wire.setSource((SMNode) source.getModel());
			wire.setSourceTerminal(sourceTerminal);		
			wire.attachSource();
			source.refreshEPSourceConnections();
		}
		if (target != null) {
			wire.detachTarget();
			target.refreshEPTargetConnections();
			wire.setTarget((SMNode) target.getModel());
			wire.setTargetTerminal(targetTerminal);
			wire.attachTarget();
			target.refreshEPTargetConnections();
		}
		if (source == null && target == null){
			wire.detachSource();
			source.refreshEPSourceConnections();
			wire.detachTarget();
			target.refreshEPTargetConnections();
			wire.setTarget(null);
			wire.setSource(null);
		}
	}

}
