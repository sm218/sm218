package org.iceberg.sm218.editor.command;

import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.iceberg.sm218.editor.editpart.SMDiagramEditPart;
import org.iceberg.sm218.editor.model.SMNode;

public class SMCreateCommand extends Command {

	protected SMDiagramEditPart parent = null;
	protected SMNode child = null;
	protected Rectangle rect;
	
	public void setParent(SMDiagramEditPart parent) {
		this.parent = parent;
	}

	public void setChild(SMNode child) {
		this.child = child;
	}
	
	public boolean canExecute() {
		return child != null && parent != null;
	}
	
	public void setLocation (Rectangle rect) {
		this.rect = rect;
	}


	public void execute() {
		if (rect != null) {
			Insets expansion = getInsets();
			if (!rect.isEmpty())
				rect.expand(expansion);
			else {
				rect.x -= expansion.left;
				rect.y -= expansion.top;
			}
			child.setLocation(rect.getLocation());
		}
		redo();
	}

	private Insets getInsets() {
		return new Insets();
	}

	public void redo() {
		parent.addChild(child);
	}

}
