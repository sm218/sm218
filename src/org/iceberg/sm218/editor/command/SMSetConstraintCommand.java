package org.iceberg.sm218.editor.command;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.iceberg.sm218.editor.editpart.SMEditPartNode;

public class SMSetConstraintCommand extends Command {
	
	protected SMEditPartNode part = null;	
	private Point newPos;
	private Dimension newSize;
	private Point oldPos;
	private Dimension oldSize;
	
	public void setPart(SMEditPartNode part) {
		this.part = part;
	}

	public void execute() {
		oldSize = part.getSize();
		oldPos  = part.getLocation();
		redo();
	}

	public void redo() {
		part.setSize(newSize);
		part.setLocation(newPos);
	}

	public void setLocation(Rectangle r) {
		setLocation(r.getLocation());
		setSize(r.getSize());
	}

	public void setLocation(Point p) {
		newPos = p;
	}

	public void setSize(Dimension p) {
		newSize = p;
	}

	public void undo() {
		part.setSize(oldSize);
		part.setLocation(oldPos);
	}

}
