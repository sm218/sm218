package org.iceberg.sm218.editor.editpart;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.FanRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ManhattanConnectionRouter;
import org.eclipse.draw2d.ShortestPathConnectionRouter;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.LayerConstants;
import org.iceberg.sm218.editor.editpolicy.SMXYLayoutEditPolicy;
import org.iceberg.sm218.editor.figure.SMDiagramFigure;
import org.iceberg.sm218.editor.model.SMActor;
import org.iceberg.sm218.editor.model.SMConnection;
import org.iceberg.sm218.editor.model.SMDiagram;
import org.iceberg.sm218.editor.model.SMNode;

public class SMDiagramEditPart extends SMEditPartNode {
	
	public final static int CRT_SHORTESTPATH = 0;
	public final static int CRT_FAN = 1;
	public final static int CRT_MANHATTAN = 2;
	public final static int CRT_ANOHER = 3;
	
	@Override
	protected IFigure createFigure() {
		/*IFigure figure = new FreeformLayer();
	
		Border border = new GroupBoxBorder("SM218");
		figure.setBorder(border);
		figure.setLayoutManager(new FreeformLayout());
		*/
		
		IFigure figure = new SMDiagramFigure();
		return figure;
	}	
	
	public void setConnectionRouter(int type) {
		ConnectionLayer connLayer = 
            (ConnectionLayer)getLayer(LayerConstants.CONNECTION_LAYER); 
		
		ConnectionRouter router;
		if(type == CRT_FAN) {
			router = new FanRouter();
		} else if(type == CRT_MANHATTAN) {
			router = new ManhattanConnectionRouter();
		} else if(type == CRT_SHORTESTPATH) {
			router = new ShortestPathConnectionRouter(getFigure());
		} else {
			router = new FanRouter();
		}
		
		connLayer.setConnectionRouter(router);
		
		refreshVisuals();
	}
	
	@Override
	protected void createEditPolicies() {		
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new SMXYLayoutEditPolicy(
				(XYLayout)getContentPane().getLayoutManager())
		);
	}
	
	@Override
	protected List<Object> getModelChildren() {
		List<Object> childrens = new ArrayList<Object>();
		childrens.addAll(((SMDiagram)getModel()).getNeurons());
		return childrens;
	}
	
	public void addChild(SMNode child) {
		((SMDiagram) getModel()).addChild(child);
		if(child instanceof SMActor) {
			((SMActor)child).setDiagram((SMDiagram) getModel());
		}
		
		addChild(createChild(child), -1);
	}

	public synchronized void nextTime() {
		((SMDiagram)getModel()).nextTime();
	}
	
	@Override
	protected Vector<SMConnection> getModelSourceConnections(){
		return new Vector<SMConnection>();
	}

	@Override
	protected Vector<SMConnection> getModelTargetConnections(){
		return new Vector<SMConnection>();
	}
	
}
