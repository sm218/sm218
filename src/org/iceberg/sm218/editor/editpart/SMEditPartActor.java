package org.iceberg.sm218.editor.editpart;

import org.eclipse.draw2d.geometry.Dimension;
import org.iceberg.sm218.editor.model.SMActor;

public class SMEditPartActor extends SMEditPartNode {

	@Override
	public SMActor getModel() {
		return (SMActor) super.getModel();
	}
	
	public Dimension getSize() {
		return new Dimension(200, 200);
	}
}
