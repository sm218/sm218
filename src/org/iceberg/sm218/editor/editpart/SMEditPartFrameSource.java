package org.iceberg.sm218.editor.editpart;

import org.eclipse.draw2d.geometry.Dimension;
import org.iceberg.sm218.editor.model.SMFrameSource;

public class SMEditPartFrameSource extends SMEditPartNode {
		
	@Override
	public SMFrameSource getModel() {
		return (SMFrameSource) super.getModel();
	}
	
	public int getHeaderHeight() {
		return 20;
	}
	
	
	public int getFrameHeight() {
		return 100; 
	}
	
	public int getFrameWidth() {
		return 100; 
	}
	
	public int getInset() {
		return 5;
	}
	
	public int getGraphWidth() {
		return 200;
	}
	
	public int getGraphHeight() {
		return getModel().getMaxOutputValue() > getFrameHeight() ? getModel().getMaxOutputValue() : getFrameHeight();
	}
	
	public Dimension getSize() {
		Dimension dim = new Dimension(0,0);
		dim.height = getHeaderHeight() + 2*getInset() + 
			( getGraphHeight() > getFrameHeight() ? getGraphHeight() : getFrameHeight());
		dim.width = 4*getInset() + getFrameWidth() + getGraphWidth();
		return dim;
	}

}
