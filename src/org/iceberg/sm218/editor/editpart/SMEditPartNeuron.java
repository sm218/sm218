package org.iceberg.sm218.editor.editpart;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.iceberg.sm218.editor.model.SMNeuron;

public class SMEditPartNeuron extends SMEditPartNode implements IPropertySource {

	protected static IPropertyDescriptor[] descriptors = null;

	public static enum props {
		PID_SIGNAL,
		PID_BACK_SIGNAL,
		PID_NUMBER_EVENTS_BTW,
		PID_NUMBER_EVENTS_P,
		PID_THRESHOLD_Q,
		PID_THRESHOLD_M,
		PID_STATE,
		PID_ID
	}
	
	static {
		descriptors = new IPropertyDescriptor[8];
		descriptors[0] = new PropertyDescriptor(props.PID_ID, "Neuron identfier");
		descriptors[1] = new PropertyDescriptor(props.PID_STATE, "Neuron state");
		descriptors[2] = new PropertyDescriptor(props.PID_SIGNAL, "Signal");
		descriptors[3] = new PropertyDescriptor(props.PID_BACK_SIGNAL, "Back off signal");
		descriptors[4] = new PropertyDescriptor(props.PID_NUMBER_EVENTS_BTW, "Number of btw events");
		descriptors[5] = new PropertyDescriptor(props.PID_NUMBER_EVENTS_P, "Number of single signals ");
		descriptors[6] = new PropertyDescriptor(props.PID_THRESHOLD_M, "Threshold for btw events");
		descriptors[7] = new PropertyDescriptor(props.PID_THRESHOLD_Q, "Threshold for single signal");
	}
	
	@Override
	public SMNeuron getModel() {
		return (SMNeuron) super.getModel();
	}

	@Override
	public Object getEditableValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		return descriptors;
	}

	@Override
	public Object getPropertyValue(Object id) {
		switch ((props)id) {
			case PID_ID: 
				return getModel().getName();
			case PID_STATE:
				return getState();
			case PID_SIGNAL:
				return getSignal();
			case PID_BACK_SIGNAL:
				return getModel().getSignalZ();
			case PID_NUMBER_EVENTS_BTW: 
				return getModel().getCounterBtw();
			case PID_THRESHOLD_M:
				return getModel().getThresholdBtw();
			case PID_NUMBER_EVENTS_P:
				return getModel().getCounterP();
			case PID_THRESHOLD_Q:
				return getModel().getThresholdQ();	
		}
		return null;
	}

	public boolean getSignal() {
		return getModel().getSignal();
	}
	
	public boolean getState() {
		return getModel().getState();
	}
	
	@Override
	public boolean isPropertySet(Object id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		// TODO Auto-generated method stub
		
	}
	

	
	
	

}
