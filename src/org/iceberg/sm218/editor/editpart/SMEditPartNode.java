package org.iceberg.sm218.editor.editpart;

import java.util.Vector;

import org.eclipse.draw2d.Animation;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.requests.DropRequest;
import org.iceberg.sm218.editor.editpolicy.SMNodeEditPolicy;
import org.iceberg.sm218.editor.figure.SMFigureFactory;
import org.iceberg.sm218.editor.figure.SMFigureNode;
import org.iceberg.sm218.editor.model.INeuroProperty;
import org.iceberg.sm218.editor.model.SMConnection;
import org.iceberg.sm218.editor.model.SMNode;

public class SMEditPartNode extends AbstractGraphicalEditPart  implements NodeEditPart {

	@Override
	protected void createEditPolicies() {
        installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new SMNodeEditPolicy());
	}
	
	@Override
	protected void refreshVisuals() {			
		Animation.markBegin();
		Point loc = getLocation();
		Dimension size = getSize();
		Rectangle r = new Rectangle(loc ,size);
		((AbstractGraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), r);
		Animation.run(400);
	}
	

	
	public Point getLocation() {
		return ((INeuroProperty)getModel()).getLocation();
	}
	
	public void setSize(Dimension size) {
		
	}
	
	public void setLocation(Point location) {
		((INeuroProperty)getModel()).setLocation(location);
		refreshVisuals();
	}

	public Dimension getSize() {
		return new Dimension(20, 20);
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			ConnectionEditPart connection) {
		return ((SMFigureNode)getFigure()).getConnectionAnchor(1+"");
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			ConnectionEditPart connection) {
		return ((SMFigureNode)getFigure()).getConnectionAnchor(2+"");
	}

	@Override
	protected IFigure createFigure() {
		return SMFigureFactory.createFigureFromEditPart(this);
	}
	
	public ConnectionAnchor getSourceConnectionAnchor(SMEditPartWire connEditPart) {
		return ((SMFigureNode)getFigure()).getConnectionAnchor(1+"");
	}

	public ConnectionAnchor getTargetConnectionAnchor(SMEditPartWire connEditPart) {
		return ((SMFigureNode)getFigure()).getConnectionAnchor(2+"");
	}
	

	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		Point pt = new Point(((DropRequest)request).getLocation());
		return ((SMFigureNode)getFigure()).getTargetConnectionAnchorAt(pt);
	}
	
	public ConnectionAnchor getSourceConnectionAnchor(Request request) {
		Point pt = new Point(((DropRequest)request).getLocation());
		return ((SMFigureNode)getFigure()).getSourceConnectionAnchorAt(pt);
	}

	public String mapConnectionAnchorToTerminal(ConnectionAnchor c){
		return ((SMFigureNode)getFigure()).getConnectionAnchorName(c);
	}

	public void connectOutput(SMConnection wire) {
		((SMNode)getModel()).connectOutput(wire);
		refreshSourceConnections();
	}

	public void connectInput(SMConnection wire) {
		((SMNode)getModel()).connectInput(wire);
		refreshTargetConnections();	
	}

	public void disconnectInput(SMConnection wire) {
		((SMNode)getModel()).disconnectInput(wire);
		refreshTargetConnections();	
	}

	public void disconnectOutput(SMConnection wire) {
		((SMNode)getModel()).disconnectOutput(wire);
		refreshSourceConnections();
	}

	@Override
	protected Vector<SMConnection> getModelSourceConnections(){
		return ((SMNode)getModel()).getSourceConnections();
	}

	@Override
	protected Vector<SMConnection> getModelTargetConnections(){
		return ((SMNode)getModel()).getTargetConnections();
	}

	
	protected ConnectionAnchor anchor = null;
	
	public void setInsteadConnectionAnchor(ConnectionAnchor anchor) {
		this.anchor = anchor;
	}
		
	public void refreshEPSourceConnections() {
		refreshSourceConnections();
	}

	
	public void refreshEPTargetConnections() {
		refreshTargetConnections();
	}
	
}
