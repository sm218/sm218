package org.iceberg.sm218.editor.editpart;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;
import org.iceberg.sm218.editor.model.SMTonic;

public class SMEditPartTonic extends SMEditPartNode implements IPropertySource {

	protected static IPropertyDescriptor[] descriptors = null;

	public static enum props {
		PID_SIGNAL,
		PID_ID,
		PID_DATA, PID_DATAIN
	}
	
	static {
		descriptors = new IPropertyDescriptor[3];
		descriptors[0] = new PropertyDescriptor(props.PID_ID, "Tonic identfier");
		descriptors[1] = new PropertyDescriptor(props.PID_SIGNAL, "Signal");
		descriptors[2] = new TextPropertyDescriptor(props.PID_DATA, "Binary data");
	}
	
	@Override
	public SMTonic getModel() {
		return (SMTonic) super.getModel();
	}

	@Override
	public Object getEditableValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		return descriptors;
	}

	@Override
	public Object getPropertyValue(Object id) {
		switch ((props)id) {
			case PID_ID: 
				return getModel().getName();
			case PID_SIGNAL:
				return getModel().getSignal();
			case PID_DATA:
				int[] data = getBData();
				String sData = "";
				for(int i=0; i<data.length; i++) {
					sData += i == 0 ? "" : ",";
					sData += data[i];
				}
				return sData;
			

		}
		return null;
	}

	@Override
	public boolean isPropertySet(Object id) {
		switch ((props)id) {
			case PID_DATA:
				return true;
		}
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if(props.PID_DATA.equals(id)) {
			String svalue = (String) value;
			String[] values = svalue.split(",");
			int[] newData = new int[values.length];
			for(int i=0; i<values.length; i++) {
				newData[i] = Integer.parseInt(values[i]);
			}
			setBData(newData);
		}
	}

	public int[] getBData() {
		return ((SMTonic)getModel()).getData();
	}

	public void setBData(int[] newData) {
		((SMTonic)getModel()).setData(newData);	
	}
	

	
	
	

}
