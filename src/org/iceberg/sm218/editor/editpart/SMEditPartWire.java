package org.iceberg.sm218.editor.editpart;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.iceberg.sm218.editor.editpolicy.SMConnectionEditPolicy;
import org.iceberg.sm218.editor.figure.SMFigureFactory;

public class SMEditPartWire extends AbstractConnectionEditPart {

	@Override
    protected IFigure createFigure() {
        Connection connection = SMFigureFactory.createNewBendableWire(this);
        return connection;
    }
	
	@Override
	protected void createEditPolicies() {
	    installEditPolicy(EditPolicy.CONNECTION_ROLE, new SMConnectionEditPolicy());
	}

}
