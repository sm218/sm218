package org.iceberg.sm218.editor.editpolicy;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.iceberg.sm218.editor.command.SMConnectionCommand;
import org.iceberg.sm218.editor.editpart.SMEditPartNode;
import org.iceberg.sm218.editor.figure.SMFigureFactory;
import org.iceberg.sm218.editor.model.SMConnection;

public class SMNodeEditPolicy extends GraphicalNodeEditPolicy {

	@Override
	protected Connection createDummyConnection(Request req) {
		if(req instanceof CreateConnectionRequest) {
			return (Connection) SMFigureFactory.createFigure(((CreateConnectionRequest)req).getNewObjectType());
		}
		return super.createDummyConnection(req);
	}
	
	@Override
	protected Command getConnectionCompleteCommand(
			CreateConnectionRequest request) {
        SMConnectionCommand command = (SMConnectionCommand)request.getStartCommand();
        command.setTarget((SMEditPartNode) getHost());
        ConnectionAnchor ctor = ((SMEditPartNode) getHost()).getTargetConnectionAnchor(request);
        if (ctor == null)
                return null;
        command.setTargetTerminal(((SMEditPartNode) getHost()).mapConnectionAnchorToTerminal(ctor));
        return command;
	}

	@Override
	protected Command getConnectionCreateCommand(CreateConnectionRequest request) {
        SMConnectionCommand command = new SMConnectionCommand();        
        command.setWire((SMConnection) request.getNewObject());
        command.setSource((SMEditPartNode) getHost());
        ConnectionAnchor ctor = ((SMEditPartNode) getHost()).getSourceConnectionAnchor(request);
        command.setSourceTerminal(((SMEditPartNode) getHost()).mapConnectionAnchorToTerminal(ctor));
        request.setStartCommand(command);
        return command;
	}

	@Override
	protected Command getReconnectSourceCommand(ReconnectRequest request) {
		//System.out.println("RECONNCET");
		return null;
	}

	@Override
	protected Command getReconnectTargetCommand(ReconnectRequest request) {
		//System.out.println("RECONNCET TARGET");
		return null;
	}

}
