package org.iceberg.sm218.editor.editpolicy;

import java.util.Iterator;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.PrecisionRectangle;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editpolicies.ResizableEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.iceberg.sm218.editor.figure.SMFigureFactory;

public class SMResizableEditPolicy extends ResizableEditPolicy {

	protected IFigure getFeedbackLayer() {
		return getLayer(LayerConstants.SCALED_FEEDBACK_LAYER);
	}
	
	@Override
	protected void showChangeBoundsFeedback(ChangeBoundsRequest request) {
		
		IFigure feedback = getDragSourceFeedbackFigure();
		
		Rectangle initialBounds = getInitialFeedbackBounds();

		PrecisionRectangle rect = new PrecisionRectangle(initialBounds.getCopy());

		rect.resize(request.getSizeDelta());
		rect.translate(request.getMoveDelta());
		
		getHostFigure().translateToAbsolute(rect);
		feedback.translateToRelative(rect);
		feedback.setBounds(rect);
	}
	
	@Override
	protected IFigure createDragSourceFeedbackFigure() {
		IFigure figure = createFigure((GraphicalEditPart) getHost(), null);
		figure.setBounds(getInitialFeedbackBounds());
		addFeedback(figure);
		return figure;
	}

	@Override
	protected Rectangle getInitialFeedbackBounds() {
		return getHostFigure().getBounds();
	}

	protected IFigure createFigure(GraphicalEditPart part, IFigure parent) {
		IFigure child = SMFigureFactory.createInitializedFigureFromEditPart(part);
		if (parent != null)
			parent.add(child);

		Rectangle childBounds = part.getFigure().getBounds().getCopy();

		IFigure walker = part.getFigure().getParent();

		while (walker != ((GraphicalEditPart) part.getParent()).getFigure()) {
			walker.translateToParent(childBounds);
			walker = walker.getParent();
		}

		child.setBounds(childBounds);

		Iterator<?> i = part.getChildren().iterator();

		// add only if it visible (edit part have this property)
		while (i.hasNext()) {
			EditPart childEp = (EditPart)i.next();
			//if(childEp instanceof SMEditPartNode && ((SMEditPartNode)childEp).isVisible())
				createFigure((GraphicalEditPart) childEp, child);
		}

		return child;
	}
	
}
