package org.iceberg.sm218.editor.editpolicy;

import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.iceberg.sm218.editor.command.SMCreateCommand;
import org.iceberg.sm218.editor.command.SMSetConstraintCommand;
import org.iceberg.sm218.editor.editpart.SMDiagramEditPart;
import org.iceberg.sm218.editor.editpart.SMEditPartNode;
import org.iceberg.sm218.editor.model.SMNode;

public class SMXYLayoutEditPolicy extends XYLayoutEditPolicy {

	public SMXYLayoutEditPolicy(XYLayout layout) {
		setXyLayout(layout);
	}
	
	@Override
	protected Command createChangeConstraintCommand(EditPart child, Object constraint) {
		SMSetConstraintCommand cmd = new SMSetConstraintCommand();		
		cmd.setPart((SMEditPartNode)child);
		cmd.setLocation((Rectangle) constraint);
		Command result = cmd;
		return result;
	}

	@Override
	protected Command getCreateCommand(CreateRequest request) {		
		SMCreateCommand create = new SMCreateCommand();
		create.setParent((SMDiagramEditPart)getHost());
		
		SMNode newPart = (SMNode) request.getNewObject();
		create.setChild(newPart);
		Rectangle constraint = (Rectangle) getConstraintFor(request);
		
		create.setLocation(constraint);
		return create;	
	}
	
    @Override
    protected EditPolicy createChildEditPolicy(EditPart child) {
            EditPolicy editPolicy = new SMResizableEditPolicy();
            return editPolicy;
    }


}
