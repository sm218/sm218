package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PrecisionPoint;

public class FixedConnectionAnchor extends AbstractConnectionAnchor {

	public FixedConnectionAnchor(SMFigureNode figure) {
		super(figure);
	}

	@Override
	public Point getLocation(Point reference) {
        // try to return nearest point for reference in case of different Figures
        IFigure figure = getOwner();

        Point point = ((SMFigureNode)figure).getAnchorPoint(reference);
  
        Point p = new PrecisionPoint(point.x ,point.y);
        getOwner().translateToAbsolute(p);
        return p;

	}


}
