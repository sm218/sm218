package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.Graphics;

public class SMDiagramFigure extends FreeformLayer {

	public SMDiagramFigure() {
		//Border border = new GroupBoxBorder("SM218");
		//setBorder(border);
		setLayoutManager(new FreeformLayout());
	}
	
	@Override
	public void paintFigure(Graphics graphics) {
		graphics.setBackgroundColor(ColorConstants.black);
		graphics.fillRectangle(getBounds());
	}
	
}
