package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.SWT;

public class SMFigure extends Figure {
	
	protected EditPart editPart = null;
	
	@Override
	public void paint(Graphics graphics) {
		if(graphics.getAntialias() != SWT.ON)
			graphics.setAntialias(SWT.ON);
		super.paint(graphics);
	}
	
	public SMFigure(EditPart ep) {
		this.editPart = ep;
	}
	
	public EditPart getEditPart() {
		return editPart;
	}


}
