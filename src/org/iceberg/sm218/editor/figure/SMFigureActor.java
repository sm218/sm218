package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.iceberg.sm218.editor.editpart.SMEditPartActor;
import org.iceberg.sm218.editor.model.SMEmotionSubsystem;
import org.iceberg.sm218.editor.model.SMKnowledgeBase;

public class SMFigureActor extends SMFigureNode {
	


	public SMFigureActor(SMEditPartActor editPart) {
		super(editPart);
		
		connectionAnchors.clear();
		outputConnectionAnchors.clear();
		inputConnectionAnchors.clear();
	
		ConnectionAnchor connectionAnchor = new ChopboxAnchor(this);
		
		setOutputConnectionAnchor(1, connectionAnchor);
		setInputConnectionAnchor(2, connectionAnchor);
		outputConnectionAnchors.addElement(connectionAnchor);
		inputConnectionAnchors.addElement(connectionAnchor);
	}
	
	@Override
	public void paintFigure(Graphics graphics) {
		Rectangle rect = getClientArea();
		graphics.setBackgroundColor(ColorConstants.gray);
		graphics.fillRectangle(rect);
		
		graphics.setFont(SMFigureNeuron.font);
		graphics.setForegroundColor(ColorConstants.black);
		
		SMEmotionSubsystem emoSys = getEditPart().getModel().getEmoSys(); 
		SMKnowledgeBase kBase = getEditPart().getModel().getKBase(); 
			
		String line1 = "Emotion: " + emoSys.getCurrentEmo();
		graphics.drawText(line1, rect.x + 10, rect.y);
		String line2 = "Images: " + kBase.imageCounter;
		graphics.drawText(line2, rect.x + 10, rect.y + 15);
		String line3 = "Problem: " + getEditPart().getModel().getProblem();
		graphics.drawText(line3, rect.x + 10, rect.y + 30);


	}
	
	@Override
	public SMEditPartActor getEditPart() {
		return (SMEditPartActor)super.getEditPart();
	}

}
