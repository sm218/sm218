package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RoutingAnimator;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.SWT;
import org.iceberg.sm218.editor.editpart.SMEditPartActor;
import org.iceberg.sm218.editor.editpart.SMEditPartFrameSource;
import org.iceberg.sm218.editor.editpart.SMEditPartNeuron;
import org.iceberg.sm218.editor.editpart.SMEditPartNode;
import org.iceberg.sm218.editor.editpart.SMEditPartTonic;
import org.iceberg.sm218.editor.editpart.SMEditPartWire;
import org.iceberg.sm218.editor.model.SMConnection;
import org.iceberg.sm218.editor.model.SMNode;

public class SMFigureFactory {

	public static Connection createNewBendableWire(SMEditPartWire editPart) {
        PolylineConnection conn = new SMPolylineConnection(editPart);
        conn.setAntialias(SWT.ON);
        conn.addRoutingListener(RoutingAnimator.getDefault());
        
        PolygonDecoration arrow = new PolygonDecoration();
		arrow.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		arrow.setScale(5, 2.5);
		conn.setTargetDecoration(arrow);

        /*conn.setSourceDecoration(new PolygonDecoration());
        conn.setTargetDecoration(new PolylineDecoration());*/
        return conn;
	}

	public static Connection createFigure(Object object) {
		
			if(object.equals(SMConnection.class)) {
				return new SMPolylineConnection();
			}
			return null;
	}

	public static IFigure createFigureFromEditPart(EditPart editPart) {
		
		IFigure figure = createInitializedFigureFromEditPart(editPart);
		
		if(figure != null && editPart instanceof SMEditPartNode) {
			String identifier = ((SMNode)((SMEditPartNode)editPart).getModel()).getName();
			figure.setToolTip(new Label(identifier));
		}
		
		return figure;
	}
	
	public static IFigure createInitializedFigureFromEditPart(EditPart editPart) {
		
		IFigure figure = null;
		if(editPart instanceof SMEditPartNeuron) {
			figure =  new SMFigureNeuron((SMEditPartNeuron)editPart);
		} else if(editPart instanceof SMEditPartTonic) {
			figure =  new SMFigureTonic((SMEditPartTonic)editPart);
		} else if(editPart instanceof SMEditPartFrameSource) {
			figure =  new SMFigureFrameSource((SMEditPartFrameSource)editPart);
		} else if(editPart instanceof SMEditPartActor) {
			figure =  new SMFigureActor((SMEditPartActor)editPart);
		}
			
		return figure;
	}
}
