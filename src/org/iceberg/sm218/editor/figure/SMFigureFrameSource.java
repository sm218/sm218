package org.iceberg.sm218.editor.figure;

import java.util.List;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.iceberg.sm218.editor.editpart.SMEditPartFrameSource;

public class SMFigureFrameSource extends SMFigureNode {

	public SMFigureFrameSource(EditPart ep) {
		super(ep);
		
		
		connectionAnchors.clear();
		outputConnectionAnchors.clear();
		inputConnectionAnchors.clear();
	
		ConnectionAnchor connectionAnchor = new ChopboxAnchor(this);
		
		setOutputConnectionAnchor(1, connectionAnchor);
		setInputConnectionAnchor(2, connectionAnchor);
		outputConnectionAnchors.addElement(connectionAnchor);
		inputConnectionAnchors.addElement(connectionAnchor);
	}
	
	protected Rectangle getHeaderArea() {
		Rectangle rect = getClientArea();
		rect.height = getEditPart().getFrameHeight();
		return rect;
	}
	
	protected Rectangle getClientAreaForFrame() {
		Rectangle rect = getClientArea();
		rect.y += getEditPart().getInset() + getEditPart().getHeaderHeight();
		rect.height = getEditPart().getFrameHeight();
		rect.width = getEditPart().getFrameWidth();
		rect.x += getEditPart().getInset();
		return rect;
	}
	
	protected Rectangle getClientAreaForGraph() {
		Rectangle rect = getClientArea();
		rect.y += getEditPart().getInset() + getEditPart().getHeaderHeight();
		rect.height = getEditPart().getGraphHeight();
		rect.width = getEditPart().getGraphWidth();
		rect.x += 3*getEditPart().getInset() + getEditPart().getFrameWidth();
		return rect;
	}

	
	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle rect = getClientArea();
		graphics.setBackgroundColor(ColorConstants.gray);
		graphics.fillRectangle(rect);
	
		rect = getClientAreaForFrame();
		
		
		graphics.setBackgroundColor(ColorConstants.darkGray);
		graphics.fillRectangle(rect);
		
		int pos = getEditPart().getModel().getOutputValue();
		
		int shift = 0;
		int shiftfull = 0;
		if(pos != 0) {
			shift = (int)((double)rect.width/(2.0*getEditPart().getModel().getMaxOutputValue()));
			shiftfull = shift*pos;
		}
		
		graphics.setForegroundColor(ColorConstants.white);
		
		graphics.drawLine(rect.x + rect.width/2, 
				rect.y, rect.x + rect.width/2, rect.y + rect.height);

		graphics.drawLine(rect.x, rect.y + rect.height/2,
				rect.x + rect.width, rect.y + rect.height/2);
		
		graphics.setBackgroundColor(ColorConstants.red);
		
		int halfSize = 2;
		graphics.fillOval(rect.x + rect.width/2 - halfSize + shiftfull, rect.y + rect.height/2 - halfSize, 
				2*halfSize, 2*halfSize);
		
		rect = getClientAreaForGraph();
		graphics.setBackgroundColor(ColorConstants.darkGray);
		graphics.fillRectangle(rect);
		graphics.setForegroundColor(ColorConstants.white);
		graphics.drawLine(rect.x, rect.y + rect.height,
				rect.x + rect.width, rect.y + rect.height);
		graphics.drawLine(rect.x, rect.y,
				rect.x, rect.y + rect.height);
		
		graphics.setForegroundColor(ColorConstants.green);
		List<Integer> valuesLst = getEditPart().getModel().getGraphList();
		
		
		
		int limitSize;
		int shiftX;
		if(rect.width >= valuesLst.size()) {
			limitSize = valuesLst.size();
			shiftX = 0;
		} else {
			limitSize = rect.width;
			shiftX = valuesLst.size() - rect.width;
		}
		int[] linePoints = new int[limitSize*2];
		
		for(int i=0; i<limitSize; i++) {
			Integer curVal = valuesLst.get(i+shiftX);
			
			linePoints[i*2]   = rect.x + i;
			linePoints[i*2+1] = rect.y + rect.height - curVal;
			
			//graphics.drawPoint(rect.x + i*shift, rect.y + rect.height/2 + curVal);
		}
		
		// draw max line
		graphics.setForegroundColor(ColorConstants.red);
		graphics.drawLine(rect.x, rect.y + rect.height - getEditPart().getModel().getMaxOutputValue(),
				rect.x + rect.width, rect.y + rect.height - getEditPart().getModel().getMaxOutputValue());
		
		// draw optimal line
		graphics.setForegroundColor(ColorConstants.white);
		graphics.drawLine(rect.x, rect.y + rect.height - getEditPart().getModel().getMaxOutputValue()/2,
				rect.x + rect.width, rect.y + rect.height - getEditPart().getModel().getMaxOutputValue()/2);	
		
		
		// and draw graph
		graphics.setForegroundColor(ColorConstants.green);
		graphics.drawPolyline(linePoints);
		
		rect = getHeaderArea();
		graphics.setFont(SMFigureNeuron.font);
		graphics.setForegroundColor(ColorConstants.black);
		
		String text = " " + getEditPart().getModel().getIdentString();
		graphics.drawText(text, rect.x, rect.y);
		
		
	
	}
	
	@Override
	public SMEditPartFrameSource getEditPart() {
		return (SMEditPartFrameSource)super.getEditPart();
	}
	
}
