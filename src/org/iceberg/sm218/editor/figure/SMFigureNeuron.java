package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.iceberg.sm218.editor.editpart.SMEditPartNeuron;

public class SMFigureNeuron extends SMFigureNode {
	
	public static Font font = new Font(null, "Verdana", 8, SWT.BOLD | SWT.ITALIC);
	
	public SMFigureNeuron(EditPart ep) {
		super(ep);
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		
		
		Rectangle rect = getClientArea();
		
		Color color = null;
		if(getEditPart().getSignal()) {
			color = new Color(null, 200, 0, 0);
		} else if(getEditPart().getState()) {
			color = new Color(null, 0, 255, 0);		
		} else {
			color = new Color(null, 0, 150, 0);	
		}
		
		graphics.setBackgroundColor(color);
		graphics.fillOval(rect);
		
		if(getEditPart()!=null) {
			String identifier = getEditPart().getModel().getName() + "";
			graphics.setFont(font);
			graphics.setForegroundColor(ColorConstants.white);
			
			int mshift = 0;
			if(identifier.length() == 1)
				mshift = 6;
			else if(identifier.length() == 2) 
				mshift = 2;
				
			graphics.drawText(identifier, rect.x + mshift, rect.y + 3);
		}
	}
	
	@Override
	public SMEditPartNeuron getEditPart() {
		return (SMEditPartNeuron)super.getEditPart();
	}
}
