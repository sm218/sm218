package org.iceberg.sm218.editor.figure;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.EllipseAnchor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;

public class SMFigureNode extends SMFigure {
	
	

	protected Hashtable<String, ConnectionAnchor> connectionAnchors = new Hashtable<String, ConnectionAnchor>(
			7);
	protected Vector<ConnectionAnchor> inputConnectionAnchors = new Vector<ConnectionAnchor>(
			2, 2);
	protected Vector<ConnectionAnchor> outputConnectionAnchors = new Vector<ConnectionAnchor>(
			2, 2);
	
	public SMFigureNode(EditPart ep) {
		super(ep);
		
		ConnectionAnchor connectionAnchor = new EllipseAnchor(this);
		
		setOutputConnectionAnchor(1, connectionAnchor);
		setInputConnectionAnchor(2, connectionAnchor);
		outputConnectionAnchors.addElement(connectionAnchor);
		inputConnectionAnchors.addElement(connectionAnchor);
	}
	
	
	
    public Point getAnchorPoint(Point ref) {
    	Point location = getLocation();
    	Dimension size = getSize();
    	
    	Point anchorPoint = new Point(location.x + size.width/2, location.y + size.height/2);
    	
        return anchorPoint;
    }

	public ConnectionAnchor getConnectionAnchor(String terminal) {
		return (ConnectionAnchor) connectionAnchors.get(terminal);
	}

	public void setInputConnectionAnchor(int i, ConnectionAnchor c) {
		connectionAnchors.put(i + "", c);
	}

	public void setOutputConnectionAnchor(int i, ConnectionAnchor c) {
		connectionAnchors.put(i + "", c);
	}
	

    public ConnectionAnchor getSourceConnectionAnchorAt(Point pt) {
            ConnectionAnchor closest = null;
            long min = Long.MAX_VALUE;

            Enumeration<ConnectionAnchor> e = getSourceConnectionAnchors().elements();
            while (e.hasMoreElements()) {
                    ConnectionAnchor c = (ConnectionAnchor) e.nextElement();
                    Point p2 = c.getLocation(pt);
                    long d = (long)pt.getDistance(p2);
                    if (d < min) {
                            min = d;
                            closest = c;
                    }
            }
            return closest;
    }

    public Vector<ConnectionAnchor> getSourceConnectionAnchors() {
            return outputConnectionAnchors;
    }


    public ConnectionAnchor getTargetConnectionAnchorAt(Point p) {
            ConnectionAnchor closest = null;
            long min = Long.MAX_VALUE;

            Enumeration<ConnectionAnchor> e = getTargetConnectionAnchors().elements();
            while (e.hasMoreElements()) {
                    ConnectionAnchor c = (ConnectionAnchor) e.nextElement();
                    Point p2 = c.getLocation(p);
                    long d = (long)p.getDistance(p2);
                    if (d < min) {
                            min = d;
                            closest = c;
                    }
            }
            return closest;
    }

    public Vector<ConnectionAnchor> getTargetConnectionAnchors() {
            return inputConnectionAnchors;
    }
    

	public String getConnectionAnchorName(ConnectionAnchor c){
        Enumeration<String> keys = connectionAnchors.keys();
        String key;
        while (keys.hasMoreElements()){
                key = (String)keys.nextElement();
                if (connectionAnchors.get(key).equals(c))
                        return key;
        }
        return null;
	}


}
