package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Color;

public class SMFigureTonic extends SMFigureNode {
	
	public SMFigureTonic(EditPart ep) {
		super(ep);
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		
		Rectangle rect = getClientArea();
		graphics.setBackgroundColor(new Color(null, 0, 0, 150));
		graphics.fillOval(rect);
		
	}
}
