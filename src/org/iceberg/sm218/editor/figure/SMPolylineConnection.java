package org.iceberg.sm218.editor.figure;

import org.eclipse.draw2d.PolylineConnection;
import org.iceberg.sm218.editor.editpart.SMEditPartWire;

public class SMPolylineConnection extends PolylineConnection {

	protected SMEditPartWire editPart;
	
	public SMPolylineConnection(SMEditPartWire editPart) {
		this.editPart = editPart;
	}

    public SMPolylineConnection() {
		// TODO Auto-generated constructor stub
	}

	public SMEditPartWire getEditPart() {
        return editPart;
    }

	
}
