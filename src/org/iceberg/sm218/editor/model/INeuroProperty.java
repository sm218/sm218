package org.iceberg.sm218.editor.model;

import org.eclipse.draw2d.geometry.Point;

public interface INeuroProperty {

	public Point getLocation();
	public void setLocation(Point point);
}
