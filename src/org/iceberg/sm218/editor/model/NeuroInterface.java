package org.iceberg.sm218.editor.model;


public interface NeuroInterface {

	boolean getSignal();

	void calculate();
	
	void printState();
	
	void next();

	void setSignalZ(boolean zpt1);

	String getName();
	
	//public List<NeuroInterface> getInnerNeurons();
}
