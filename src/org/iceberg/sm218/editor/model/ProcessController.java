package org.iceberg.sm218.editor.model;

import org.eclipse.ui.PlatformUI;
import org.iceberg.sm218.editor.SMEditor;

public class ProcessController {

	public final static int ACTIVE_STATE = 0;
	public final static int INACTIVE_STATE = 1;
	
	private volatile int cstate = INACTIVE_STATE;
	
	private SMEditor editor;
	
	public ProcessController(SMEditor smEditor) {
		this.editor = smEditor;
	}
	
	public int getControllerState() {
		synchronized (ProcessController.class) {
			return cstate;
		}
	}

	public void setState(int activeState) {
		synchronized (ProcessController.class) {
			if(cstate == activeState)
				return;
			
			if(activeState == ACTIVE_STATE) {
				thread = new Thread(runnable);
				cstate = ACTIVE_STATE;
				thread.start();
			} else if(activeState == INACTIVE_STATE) {
				this.cstate = INACTIVE_STATE;
				try {
					if(thread.getState() == Thread.State.RUNNABLE) {
						thread.wait();
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				thread = null;
			}
		}
	}
	
	private Thread thread;

	private MiniProcess runnable = new MiniProcess();
	
	class MiniProcess implements Runnable {
	
		@Override
		public void run() {
			while(getControllerState() == ACTIVE_STATE) {
				/*try {
					Thread.sleep(100, 0);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}*/
				
				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
					public void run() {
						editor.getDiagramEditPart().nextTime();
						editor.getDiagramEditPart().getFigure().repaint();						
					}
				});
				
			}
		}
		
	};
}
