package org.iceberg.sm218.editor.model;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class SMActor extends SMNode implements NeuroInterface {

	private static final long serialVersionUID = 1L;
	
	public static final int defCapacity = 3;
	
	private int capacity = defCapacity;
	
	private String problem = null;
	
	//private SMImage currentImage;
	private SMImage nextImage;
	
	private SMImage oldImage;
	private SMBinAction oldAction;
	
	private Random rand = new Random();
	
	//private SMBinAction currentAction;
	private SMBinAction nextAction;
	
	private SMDiagram diagram; 
	
	private SMKnowledgeBase kbase = new SMKnowledgeBase();
	
	private SMEmotionSubsystem emoSys= new SMEmotionSubsystem();

	public SMActor(int capacity) {
		this.capacity = capacity;
		oldAction = SMBinAction.createEmptyBinAction(capacity);
		nextAction = SMBinAction.createEmptyBinAction(capacity);
		emoSys.setCapacity(capacity);
	}
	
	public SMActor() {
		this(defCapacity);
	}
	
	public int getCapacity() {
		return this.capacity;
	}
	
	public void setDiagram(SMDiagram diagram) {
		this.diagram = diagram;
		this.oldImage = SMImage.createImage(diagram);
		this.nextImage = null;
	}
	
	public SMDiagram getDiagram() {
		return this.diagram;
	}
	
	@Override
	public boolean getSignal() {
		return false;
	}

	boolean isInit = false;
	
	public SMKnowledgeBase getKBase() {
		return this.kbase;
	}
	
	public SMEmotionSubsystem getEmoSys() {
		return this.emoSys;
	}
	
	public String getProblem() {
		if(problem != null)
			return problem;
		
		return "no";
	}
	
	@Override
	public void calculate() {
		if(!isInit) {
			if(getFrameSource()!=null) { 
				emoSys.setCapacity(getFrameSource().getCapacity());
				isInit = true;
				problem = null;
			} else {
				problem = "Can't find source.";
				return;
			}
		}
		
		// установим эмоциоанльную оценку системы
		// 1. получим образ сети
		nextImage = SMImage.createImage(getDiagram());
		
		// 2. получим сигнал с датчиков
		SMBinAction sensors = getFrameSourceSignals();
		
		// 3. установим образу эмоциональную оценку
		emoSys.setEvalForImage(nextImage, sensors);
		
		// 4. и такую же оценку устаноим системе
		emoSys.setEval(nextImage.getEval());
		
		// добавим образ в базу
		emoSys.setEmotion(sensors);
		int result = kbase.processTriplet(oldImage, oldAction, nextImage);
		if(result == SMKnowledgeBase.WAS_ADDED_TO_BASE || result == SMKnowledgeBase.WAS_REPLACED_IN_BASE)
			emoSys.processImage(nextImage, sensors);
	
		//сохраним эти значения как старые поскольку сейчас будем
		// генерировть новые
		oldAction = nextAction;
		oldImage = nextImage;
		
		fgSetBestByEmotionSignalForProtoImage(nextImage);
	}
	
	
	

	public SMBinAction generateRandomBinAction() {
		boolean[] signals = new boolean[getCapacity()];
		
		for(int i=0; i<signals.length; i++)
			signals[i] = rand.nextBoolean();
			
		return new SMBinAction(signals);
	}
	
	@Override
	public void printState() {	
	}

	@Override
	public void next() {
	}
	
	@Override
	public void setSignalZ(boolean zpt1) {

	}

	public SMBinAction getSignals() {
		return nextAction;
	}

	protected SMBinAction getFrameSourceSignals() {
		SMFrameSource frameSource =  getFrameSource();
		if(frameSource!=null)
			return frameSource.getOutputSignals();
		return null;
	}
	
	protected SMFrameSource getFrameSource() {
		Enumeration<SMConnection> elementss = inputs.elements();
		while (elementss.hasMoreElements()) {
			SMConnection connection = elementss.nextElement();
			NeuroInterface node = ((NeuroInterface)connection.getSource());
			if(node instanceof SMFrameSource) {
				return ((SMFrameSource)node);
			}
		}
		return null;
	}
	
	/* ============= functions - generators ====================*/
	
	/**
	 * Устанвливает слуайный сигнал управления
	 * 
	 */
	protected void fgSetRandomAction() {
		nextAction = generateRandomBinAction(); 
	}
	
	/**
	 * Устанвливает пустой сигнал управления
	 * 
	 */
	protected void fgSetEmptyAction() {
		nextAction = SMBinAction.createEmptyBinAction(getCapacity());
	}
	
	/** 
	 * 
	 * Устанавливает сигнал, которого еще не было для
	 * данного прообраза в соответствии с разрядностью
	 * сигнала.
	 * 
	 * Возвращает False, когда все варианты сигналов
	 * уже есть базе знаний для данного прообраза.
	 * 
	 */
	protected boolean fgSetNewSignalForProtoImage(SMImage image) {
		
		Map<SMBinAction, SMImage> actionMap = kbase.getActionsForProtoImage(image);
		
		if(actionMap == null) {
			fgSetRandomAction();
			return true;
		}
		
		int tryCounter = 0;
		boolean action[] = SMBinAction.createRandomSignalPack(getCapacity());
		while(isExistsInActionMap(action, actionMap)) {
			SMBinAction.nextRandomSignalPack(action);
			
			// Если количество попыток велико, то воспользуемся другой стратегией поиска значений
			if(tryCounter>20) {
				boolean maxAction[] = SMBinAction.createFullSignalPack(getCapacity());
				action = SMBinAction.createEmptySignalPack(getCapacity());
				while(!SMBinAction.isEqualsSignalPacks(action,maxAction)) {
					if(!isExistsInActionMap(action, actionMap)) {
						nextAction = new SMBinAction(action);
						return true;
					}
					SMBinAction.incrementSignalPack(action);
				}
				fgSetBestByEmotionSignalForProtoImage(image);
				return true;
			}
			tryCounter++;
		}
		nextAction = new SMBinAction(action);
		return true;		
	}
	
	/**
	 * 
	 * Устанавливает сигнал, который может привести к наилучшей
	 * эмоциональной оценки для данного прообраза.
	 * 
	 * Устанавливает random, если для данного прообраза 
	 * в базе ничего нет.
	 * 
	 *  Скорее всего неправильно устанавливать первый попавшийся
	 *  сигнал, если ничего лучше нет.
	 * 
	 * 
	 */
	protected void fgSetBestByEmotionSignalForProtoImage(SMImage image) {
		Map<SMBinAction, SMImage> actionMap = kbase.getActionsForProtoImage(image);
		
		if(actionMap == null) {
			fgSetRandomAction();
			return;
		}
		
		Iterator<Entry<SMBinAction, SMImage>> entries = actionMap.entrySet().iterator();
	
		Entry<SMBinAction, SMImage> bestEntry = null;
		int bestEmotion = 0;
		while (entries.hasNext()) {
			Entry<SMBinAction, SMImage> entry = entries.next();
			if(bestEntry == null) {
				bestEntry = entry;					
			} else {
				
				if(entry.getValue().getEval() > bestEmotion) {
					bestEmotion = entry.getValue().getEval(); 
					bestEntry = entry;					
				}
			
			}
		}	
		
		// Теперь смотрим, если прогнозируемое состояние будет хуже текущего, и 
		// в базе заполнены не все действия, то выбираем рандом.
		if((emoSys.getEval() >= bestEmotion && actionMap.size() < getMaxActions()) )
			fgSetNewSignalForProtoImage(image);
		else
			nextAction = bestEntry.getKey(); 
		// В случае, когда все варианты action's перебрали, база 
		// заполнена не лучшими прообразами, то мы можем попытаться нарастить сеть.
	}
	
	protected int getMaxActions() {
		return SMBinAction.getMaxValue(getCapacity());
	}
	
	protected boolean isExistsInActionMap(boolean[] action, Map<SMBinAction, SMImage> actionMap) {
		Iterator<SMBinAction> actions = actionMap.keySet().iterator();
		while (actions.hasNext()) {
			SMBinAction smBinAction = (SMBinAction) actions.next();
			if (smBinAction.equals(action))
				return true;
		}	
		return false;
	}
	
	/*============== функционал для новой подсистемы эмоций =============*.
	 * 
	 */
	
	/**
	 * 
	 * Устанавливает сигнал, который может привести к наилучшей
	 * эмоциональной оценки для данного прообраза.
	 * 
	 * Устанавливает random, если для данного прообраза 
	 * в базе ничего нет.
	 * 
	 *  Скорее всего неправильно устанавливать первый попавшийся
	 *  сигнал, если ничего лучше нет.
	 * 
	 * 
	 */
	protected void _fgSetBestByEmotionSignalForProtoImage(SMImage image) {
		Map<SMBinAction, SMImage> actionMap = kbase.getActionsForProtoImage(image);
		
		if(actionMap == null) {
			fgSetRandomAction();
			return;
		}
		
		Iterator<Entry<SMBinAction, SMImage>> entries = actionMap.entrySet().iterator();
	
		Entry<SMBinAction, SMImage> bestEntry = null;
		int bestEmotion = Integer.MIN_VALUE;
		while (entries.hasNext()) {
			Entry<SMBinAction, SMImage> entry = entries.next();
			if(bestEntry == null) {
				bestEntry = entry;
				bestEmotion = emoSys.getEmoDiv(entry.getValue());
			} else {
				int emo = emoSys.getEmoDiv(entry.getValue());
				if(emo > bestEmotion) {
					bestEmotion = emo; 
					bestEntry = entry;					
				}
			
			}
		}	
		
		// Теперь смотрим, если прогнозируемое состояние будет хуже текущего, и 
		// в базе заполнены не все действия, то выбираем рандом.
		if((emoSys.getEmoDiv(bestEntry.getValue()) >= bestEmotion && actionMap.size() < getMaxActions()) )
			fgSetNewSignalForProtoImage(image);
		else
			nextAction = bestEntry.getKey(); 
		// В случае, когда все варианты action's перебрали, база 
		// заполнена не лучшими прообразами, то мы можем попытаться нарастить сеть.
	}

}
