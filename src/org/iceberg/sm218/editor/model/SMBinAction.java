package org.iceberg.sm218.editor.model;

import java.io.Serializable;
import java.util.Random;

public class SMBinAction implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Random random = new Random();
	private boolean[] packedSignals;
	
	public SMBinAction(boolean[] packedSingals) {
		this.packedSignals = packedSingals;
	}
	
	public SMBinAction(int value, int capacity) {
		this.packedSignals = valueToBooleanArray(value, capacity);
	}
	
	public boolean[] getSignals() {
		return this.packedSignals;
	}
	
	public int getCapacity() {
		return packedSignals.length;
	}
	
	@Override
	public String toString() {
		return binToString(packedSignals);
	}
		
	@Override
	public boolean equals(Object object) {
		if(object instanceof SMBinAction) 
			object = ((SMBinAction)object).getSignals();
		
		if(object instanceof boolean[]) {
			boolean[] inSignals = (boolean[])object;
			
			if(inSignals.length != packedSignals.length)
				return false;
			
			for(int i=0; i<inSignals.length; i++) {
				if(inSignals[i] != packedSignals[i])
					return false;
			}
				
			return true;
		}
		return false;
	}

	
	@Override
	public int hashCode() {
		return actionsToIntValue(this);
	}
	
	public int getValue() {
		return booleanArrayToIntValue(packedSignals);
	}
	
	public int getValueSigned() {
		return booleanArrayToIntValueSigned(packedSignals);
	}
	
	/* ================== helper methods ======================*/
	
	public static SMBinAction createEmptyBinAction(int capacity) {
		return new SMBinAction(createEmptySignalPack(capacity));
	}
	
	public static boolean[] createEmptySignalPack(int capacity) {
		return createSignalPack(capacity, false);
	}
	
	public static SMBinAction createFullBinAction(int capacity) {
		return new SMBinAction(createFullSignalPack(capacity));
	}
	
	public static boolean[] createFullSignalPack(int capacity) {
		return createSignalPack(capacity, true);
	}

	public static boolean isEqualsSignalPacks(boolean[] sPack1, boolean[] sPack2) {
		if(sPack1.length != sPack2.length)
			return false;

		for(int i=0; i<sPack1.length; i++) {
			if(sPack1[i] != sPack2[i])
				return false;
		}
		
		return true;
	}
	
	public static boolean[] createSignalPack(int capacity, boolean val) {
		boolean[] emptySignalPack = new boolean[capacity];
		for(int i=0; i<emptySignalPack.length; i++)
			emptySignalPack[i] = val;
		return emptySignalPack;
	}
	
	public static void incrementSignalPack(boolean[] signalPack) {
		// 1. check - increment is possible and find first false element?
		int i=0;
		for(; i<signalPack.length; i++) {
			if(signalPack[i] == false)
				break;
			if(i == signalPack.length - 1 && signalPack[i] == true)
				throw new UnsupportedOperationException("Overflow signal pack due to increment operation!");
		}
			
		// 2. Set true to first false element and set false to all previously
		signalPack[i--] = true;
		
		for(; i >= 0; i--) 
			signalPack[i] = false;
	}
	
	public static String binToString(boolean[] signals) {
		String string = "";
		for(int i = 0; i<signals.length; i++) 
			string += signals[i] ? "1" : "0";
		return string;
	}
	
	public static int actionsToIntValue(SMBinAction binAction) {
		return booleanArrayToIntValue(binAction.getSignals());
	}
	
	public static int booleanArrayToIntValue(boolean[] signals) {
		return _booleanArrayToIntValueSigned(signals, false);
	}
	
	public static int _booleanArrayToIntValueSigned(boolean[] signals, boolean isSigned) {
		int value = 0;
		for(int i = isSigned ? 1 : 0; i<signals.length; i++) {
			if(signals[i]) 
				value += (int) Math.pow(2, signals.length -1 - i);
		}
		
		if(isSigned)
			value = signals[0] ? -value : value;
		
		return value;		
	}
	
	public static int booleanArrayToIntValueSigned(boolean[] signals) {
		return _booleanArrayToIntValueSigned(signals, true);
	}
	
	public static int getMaxValue(SMBinAction binAction) {
		return getMaxValue(binAction.getSignals());
	}
	
	public static int getMaxValue(boolean[] signals) {
		return getMaxValue(signals.length);
	}
	
	public static int getMaxValue(int capacity) {
		return (int) Math.pow(2, capacity)-1;
	}


	public static boolean[] valueToBooleanArray(int value, int capacity) {
		boolean[] signals = null;
		int absValue = Math.abs(value);
		int maxValue = value<0 ? getMaxValue(capacity-1) : getMaxValue(capacity);
		if(absValue > maxValue) {
			throw new UnsupportedOperationException("Value " + value + " is greater than max possible value " + maxValue + " !");
		} else {
			signals = new boolean[capacity]; 
			int limit = value<0 ? capacity -1: capacity;
			for(int i = 0; i < limit; i ++) {
			    signals[capacity - i - 1] = ((absValue & 1) == 0 ? false : true);
			    absValue >>= 1;
			}
		}
 		
 		if(value<0) 
 			signals[0] = true;
 		
 		return signals;
	}

	public static boolean[] createRandomSignalPack(int capacity) {
		boolean action[] = new boolean[capacity];
		for(int i=0; i<capacity; i++)
			action[i] = random.nextBoolean();
		return action;
	}	

	public static boolean[] nextRandomSignalPack(boolean[] signalPack) {
		for(int i=0; i<signalPack.length; i++)
			signalPack[i] = random.nextBoolean();
		return signalPack;
	}


}
