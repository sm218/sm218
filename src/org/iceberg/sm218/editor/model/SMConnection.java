package org.iceberg.sm218.editor.model;

import java.io.Serializable;

public class SMConnection implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected SMNode source, target;	
	protected String sourceTerminal, targetTerminal;

	public void attachSource(){
		if (getSource() == null || getSource().getSourceConnections().contains(this))
			return;
		getSource().connectOutput(this);
	}
	
	public void attachTarget(){
		if (getTarget() == null || getTarget().getTargetConnections().contains(this))
			return;
		getTarget().connectInput(this);
	}
	
	public void detachSource(){
		if (getSource() == null)
			return;
		getSource().disconnectOutput(this);
	}


	
	/**
	 * @param sourceTerminal the sourceTerminal to set
	 */
	public void setSourceTerminal(String sourceTerminal) {
		this.sourceTerminal = sourceTerminal;
	}

	/**
	 * @param targetTerminal the targetTerminal to set
	 */
	public void setTargetTerminal(String targetTerminal) {
		this.targetTerminal = targetTerminal;
	}

	public String getTargetTerminal(){
		return targetTerminal;
	}
	
	/**
	 * @param source the source to set
	 */
	public void setSource(SMNode source) {
		this.source = source;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(SMNode target) {
		this.target = target;
	}

	public void detachTarget(){
		if (getTarget() == null)
			return;
		getTarget().disconnectInput(this);
	}

	public SMNode getSource(){
		return source;
	}
	
	public SMNode getTarget(){
		return target;
	}
	
	
	public String getSourceTerminal(){
		return sourceTerminal;
	}

}
