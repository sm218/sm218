package org.iceberg.sm218.editor.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;

public class SMDiagram  implements Serializable, INeuroProperty {
	private static final long serialVersionUID = 1L;
	
	private List<NeuroInterface> brainList = new ArrayList<NeuroInterface>();
	public void addNeuron(NeuroInterface neuron) {
		brainList.add(neuron);
	}
	
	private void calculate() {
		// здесь считаются только нейроны
		for(NeuroInterface neuron: brainList)
			if(!(neuron instanceof SMActor) && !(neuron instanceof SMFrameSource))
				neuron.calculate();
	}
	
	private void next() {
		// здесь считаются только нейроны
		for(NeuroInterface neuron: brainList)
			if(!(neuron instanceof SMActor) && !(neuron instanceof SMFrameSource))
				neuron.next();
	}
	
	public void nextTime() {
		//  FrameSource
		for(NeuroInterface neuron: brainList)
			if(neuron instanceof SMFrameSource)
				neuron.calculate();	
		
		// отключим крайние нейроны, если на них был сигнал
		for(NeuroInterface neuron: brainList)
			if(neuron instanceof SMNeuron && ((SMNeuron)neuron).isLeft() && ((SMNeuron)neuron).getSignal()) {
				neuron.setSignalZ(true);	
			}
		
		// затем считаем нейроны
		calculate();
		next();
		
		//  запускаем Actor
		for(NeuroInterface neuron: brainList)
			if(neuron instanceof SMActor)
				neuron.calculate();	
	}
	public List<NeuroInterface> getNeurons() {
		return brainList;
	}

	public void addChild(SMNode child) {
		addNeuron((NeuroInterface)child);
	}

	@Override
	public Point getLocation() {
		return new Point(0,0);
	}

	@Override
	public void setLocation(Point point) {
	}
	
}
