package org.iceberg.sm218.editor.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SMEmotionSubsystem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int eval = 0;
	private int capacity = 0;
	
	Map<SMImage, Integer> images = new HashMap<SMImage, Integer>();  
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
		this.eval = 0;
	}
	
	public int getMaxEmotion() {
		return SMBinAction.getMaxValue(getCapacity())/2;
	}
	
	public int getCapacity() {
		return this.capacity;
	}
	
	public int getEmotionLimit() {
		return 0;
	}
	
	public boolean systemIsDead() {
		if(getEval() == 0)
			return true;
		return false;
	}
	
	public int getEval() {
		return this.eval;
	}
	
	public void setEval(int eval) {
		this.eval = eval;
	}

	/**
	 * 
	 * Подсистема эмоций априори знает, что нулевое
	 * состояние системы самое лучшее (нулевое отклонение от цели).
	 * 
	 * В общем случае это не верно и должна быть мотивация.
	 * 
	 * @param image
	 */
	public void setEvalForImage(SMImage image, SMBinAction outSensorSigs) {
		int valueFromSensors = Math.abs(outSensorSigs.getValue());

		int emotion = valueFromSensors >= getMaxEmotion() ? 
				Math.abs(2*getMaxEmotion() - valueFromSensors) :
					valueFromSensors;
		image.setEval(emotion);

	}

	public void add(SMImage nextImage) {
		if(images.containsKey(nextImage)) {
			Integer imgEmo = images.get(nextImage);
			if(imgEmo != nextImage.getEval())
				System.out.println("Emotion diff");
		} else {
			images.put(nextImage, nextImage.getEval());
		}
	}
	
	/* ================= another emo subsystem ========================*/

	public static final int MAX_EMO_STACK = 500;
	
	private List<Integer> emoStack = new ArrayList<Integer>(MAX_EMO_STACK);
	
	// Оценка образов, попавших в базу
	private Map<SMImage, Integer> emotionDivs = new HashMap<SMImage, Integer>();
	
	public void processImage(SMImage nextImage, SMBinAction sensors) {
		
		// даем оценку образу
		int emotion = getCurrentEmo() - getPrevEmo();
		
		if(getEmoDiv(nextImage)!=null) {
			System.out.println("Emotion incident in emoSys.");
			emotionDivs.remove(nextImage);
		
		} 
		emotionDivs.put(nextImage, emotion);	
	} 
	
	public Integer getCurrentEmo() {
		return emoStack.get(emoStack.size()-1); 
	}
	
	protected Integer getPrevEmo() {
		return emoStack.get(emoStack.size()-2); 
	}
	
	int prevSensors = 0;
	
	public void setEmotion(SMBinAction sensors) {
		int valueFromSensors = Math.abs(sensors.getValue());
		int middleValue =  SMBinAction.getMaxValue(getCapacity())/2;
		int emotion = valueFromSensors >= middleValue ? 
				Math.abs(2*middleValue - valueFromSensors) : valueFromSensors;
				// why?
		int nemotion = emotion - prevSensors;
		prevSensors = emotion;
		
		int lastEmo = getCurrentEmo() + nemotion;
		setEmotion(lastEmo);
	}
	
	protected void setEmotion(Integer emo) {
		if(emoStack.size()>MAX_EMO_STACK) {
			emoStack.remove(0);
		}
		emoStack.add(emo);
	}
	
	public Integer getEmoDiv(SMImage image) {
		return emotionDivs.get(image);
	}

	public SMEmotionSubsystem() {
		this.emoStack.add(0);
		this.emoStack.add(0);
	}
	
}
