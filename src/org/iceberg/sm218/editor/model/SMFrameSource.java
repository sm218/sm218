package org.iceberg.sm218.editor.model;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class SMFrameSource extends SMNode implements NeuroInterface {

	private static final long serialVersionUID = 1L;
	
	public static final int defSensors = 5;

	private int sensors = defSensors;  
	
	private SMBinAction signalPack = new SMBinAction(0,defSensors);
	private SMBinAction nextSignalPack;
	private int currentIndex = 0;
	
	private int hValue = 0;
	
	private int assignValue = 0;
	
	private static int GRAPH_STACK_SIZE = 201;
	
	private List<Integer> graphStack = new ArrayList<Integer>(GRAPH_STACK_SIZE); 
	
	public SMFrameSource() {
		// TODO: получить актуальный образ с актульным размером из
		//       SMActor
		nextSignalPack =  SMBinAction.createEmptyBinAction(sensors);
	}
	
	@Override
	public boolean getSignal() {
		return nextSignalPack.getSignals()[currentIndex++];
	}
	
	public int getMaxOutputValue() {
		return SMBinAction.getMaxValue(sensors);
	}
	
	public int getMaxOutputValueSigned() {
		return SMBinAction.getMaxValue(sensors-1);
	}

	public int getOutputValue() {
		return signalPack.getValue();
	}
	
	public SMBinAction getOutputSignals() {
		return nextSignalPack;
	}
	
	private void setOutputValue(int value) {
		this.nextSignalPack = new SMBinAction(value, sensors);
	}
	
	public int dvfFirst() {
		hValue++;	
		if(hValue >= getMaxOutputValue()) 
			hValue =  1 - getMaxOutputValue();
		return hValue;
	}
	
	
	double dvfSinusField = 0;
	int deb = 0;
	public int dvfSinus() {
	
	/*if(dvfSinusField == 3)
		hValue = 0;
	else if(dvfSinusField == 0)
		hValue = 3;

		dvfSinusField = hValue;
		return hValue;*/
		//dvfSinusField += 0.1;
		//hValue = (int)((Math.round(Math.sin(dvfSinusField)*((double)getMaxOutputValue()))) + getMaxOutputValue())/2;
		
		hValue = 0;
		return hValue;
	}
	
	public int defaultValueFunction() {
		// dvfFirst
		return dvfSinus();
	}
	
	@Override
	public void calculate() {
		// сохраняем старый сигнал 
		signalPack = nextSignalPack;
		
		// стандартная возмущающая функция
		defaultValueFunction();

		// попробуем уменьшить угловое отклонение
		SMBinAction binAction = getActorSignals();
		if(binAction == null) {
			//System.out.println("Can't find actors: " + hValue);
		} else {
			int valdev = binAction.getValueSigned();
			assignValue += valdev; 
		}
			// совместим поданный сигнал и сигнал от возмущения
			// TODO: Move to another function
			//assignValue = binAction.getValue();
			
			if(assignValue > getMaxOutputValue()/2)
				//assignValue = getMaxOutputValue();
				assignValue = getMaxOutputValue()/2;
			else if(assignValue < -getMaxOutputValue()/2) 
				assignValue = -getMaxOutputValue()/2;
				//assignValue = -getMaxOutputValue();
			
			hValue += assignValue;
			
			// скорректируем превышение максимального и минимального значений
			if(hValue > getMaxOutputValue()) {
				hValue = getMaxOutputValue();
			} else if(hValue < 0) {
				hValue = 0;		
			}
			
			//System.out.println("Actor was finded: " + binAction.toString() + ", real value: " + hValue);
			
		
		setOutputValue(hValue);
		
		graphStack.add(hValue);
		if(graphStack.size() >= GRAPH_STACK_SIZE) {
			graphStack.remove(0);
		}
		
		
		// сбросим индексдля нейронов
		currentIndex = 0;
	}
	
	protected SMBinAction getActorSignals() {
		Enumeration<SMConnection> elementss = inputs.elements();
		while (elementss.hasMoreElements()) {
			SMConnection connection = elementss.nextElement();
			NeuroInterface node = ((NeuroInterface)connection.getSource());
			if(node instanceof SMActor) {
				return ((SMActor)node).getSignals();
			}
		}
		return null;
	}

	@Override
	public void printState() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void next() {
	
		
	}

	@Override
	public void setSignalZ(boolean zpt1) {
		// TODO Auto-generated method stub
		
	}

	public int getSensorsCounter() {
		return sensors;
	}

	public List<Integer> getGraphList() {
		return graphStack;
	}

	public String getIdentString() {
		String ident = "Value: " + getOutputValue() + ", Bin: " + signalPack.toString();
		return ident;
	}

	public int getCapacity() {
		return sensors;
	}

}
