package org.iceberg.sm218.editor.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * Image - list of all active SMNeuron
 * 
 * @author iceberg
 *
 */
public class SMImage implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SMNeuron> neurons = new ArrayList<SMNeuron>();
	
	private int hashCode;
	
	private int eval;
	
	
	
	/**
	 * Создает образ из текущей сети
	 * 
	 * @return
	 */
	public static SMImage createImage(SMDiagram diagram) {
		List<NeuroInterface> pNeurons = diagram.getNeurons();
		
		SMImage image = new SMImage();
	
		
		if(!pNeurons.isEmpty()) {
			Iterator<?> nIter = pNeurons.iterator();
			while (nIter.hasNext()) {
				NeuroInterface neuroInterface = (NeuroInterface)nIter.next();
				if(neuroInterface instanceof SMNeuron && neuroInterface.getSignal()) {
					image.neurons.add((SMNeuron)neuroInterface);
				}
				
			}
		}

		Collections.sort(image.neurons, new Comparator<SMNeuron>() {

			@Override
			public int compare(SMNeuron neuron1, SMNeuron neuron2) {
				return neuron1.getCode() - neuron2.getCode();
			}
			
		});
		
		image.hashCode = 0;
		for(int i = 0; i<image.neurons.size(); i++) {
			SMNeuron neuron = image.neurons.get(i);
			image.hashCode += neuron.getCode()*(i+1);
		}
		
			
		return image;
	}
	
	public List<SMNeuron> getNeurons() {
		return neurons;
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
	
	@Override
	public boolean equals(Object image) {
		if(image instanceof SMImage) {
			SMImage smImage = (SMImage) image;
			
			List<SMNeuron> smNeurons = smImage.getNeurons();
			
			if(smNeurons.size() != neurons.size())
				return false;
			
			if(smNeurons.isEmpty() && neurons.isEmpty())
				return true;
			
			// compare all neurons
			for(int i=0; i<smNeurons.size(); i++) {
				SMNeuron inNeuro = smNeurons.get(i);
				if(!neurons.contains(inNeuro))
					return false;
			}
			
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		String str = "[";
		if(!neurons.isEmpty()) {
			for(int i=0; i<neurons.size(); i++) {
				str += neurons.get(i).getCode();
				if(i<neurons.size()-1)
					str += ", ";
			}
		}
		str +="]";
		return str;
	}
	
	
	public void setEval(int eval) {
		this.eval = eval;
	}
	
	public int getEval() {
		return eval;
	}
}
