package org.iceberg.sm218.editor.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class SMKnowledgeBase implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public final static int DEF_TRIPLET_MEMORY_SIZE = 5000;
	
	public final static int DEF_IMAGE_RECOGNITION_LIMIT = 50;

	public int imageRecogniyionCounter = DEF_IMAGE_RECOGNITION_LIMIT;
	
	public int imageCounter = 0;
	
	public static final int IN_TRIPLET_MEMORY = 0;
	public static final int WAS_ADDED_TO_BASE = 1;
	public static final int WAS_REPLACED_IN_BASE = 2;
	public static final int IN_BASE = 3;
	
	// Порог распознавания образ существует для того, чтобы
	// отсеивать не закономерные образы. Но если кратковременная
	// база образов переполнена и приходитновый образ, которого
	// раньше не было, то это значит, что в кратковременной
	// памяти существует образ, который не обновлялся так давно,
	// (в течении tripletMemoryLimit тактов) что может
	// быть удален из кратковременной памяти.
	
	//
	private int tripletMemoryLimit = DEF_TRIPLET_MEMORY_SIZE;
		
	private Map<Triplet, Integer> tripletMemory = new HashMap<Triplet, Integer>();
	
	private List<Triplet> tripletStack = new ArrayList<Triplet>(tripletMemoryLimit);
	
	private Map<SMImage, Map<SMBinAction, SMImage>> base = new HashMap<SMImage, Map<SMBinAction, SMImage>>();
	
	public int processTriplet(SMImage protoImage, SMBinAction action, SMImage resultImage) {
		Triplet triplet = new Triplet(protoImage, action, resultImage);
		
		Integer flag = tripletMemory.get(triplet);
	
		// flag == null => триплет не существует ни в базе, не в короткой памяти,
		// добавим его в короткую памяти 
		if(flag == null) {
			
			// Проверим не переполнилась ли короткая память...
			if(tripletStack.size() == tripletMemoryLimit) {
				Object object = tripletStack.get(0);
				tripletStack.remove(0);
				tripletMemory.remove(object);
			}
		
			// добавим триплет в короткую память
			tripletStack.add(triplet);
			tripletMemory.put(triplet, 1);
			
			return IN_TRIPLET_MEMORY;
		} 
		
		// Если триплет в короткой памяти
		if(flag < imageRecogniyionCounter) {
			tripletMemory.remove(triplet);
			tripletMemory.put(triplet, ++flag);

			// порог превышен, можно добавлять в базу
			if(flag == imageRecogniyionCounter) {
				Map<SMBinAction, SMImage> actionMap = base.get(protoImage);
				if(actionMap == null) {
					actionMap = new HashMap<SMBinAction, SMImage>();
					base.put(protoImage, actionMap);
				}
				
				SMImage image = actionMap.get(action);
				if(image == null) {
					actionMap.put(action, resultImage);
					imageCounter++;
					return WAS_ADDED_TO_BASE;
				} else {
					//imageRecogniyionCounter += imageRecogniyionCounter/10;
					System.out.println("The incident displacement of " +
							"the image in the database. " +
							"Threshold was raised to " + imageRecogniyionCounter);
					actionMap.remove(action);
					actionMap.put(action, resultImage);
					return WAS_REPLACED_IN_BASE;
				}
			}
			return IN_TRIPLET_MEMORY;
		}
		return IN_BASE;
	}
	
	public Map<SMBinAction, SMImage> getActionsForProtoImage(SMImage image) {
		return base.get(image);
	}

}
