package org.iceberg.sm218.editor.model;

import java.io.Serializable;
import java.util.Enumeration;

public class SMNeuron extends SMNode  implements Serializable, NeuroInterface {
	private static final long serialVersionUID = 1L;
	
	private int N = 0; //число событий btw
	private int M = 3; //порог // Tc132 3  //Tc160 2
	
	private int P = 0; // число единичных сигналов из предыстории
	private int Q = 3; // порог//Tc132 3 //Tc160 2
	
	
	private boolean Opt1 = false; // текущий сигнал на выхое
	private boolean Zpt1 = false; // текущий сигнал на выхое\
	private boolean Zt = false; // текущий сигнал на выхое
	private boolean Ot = false; // текущий сигнал на выхое
	private boolean Stw = false; // текущий выходной отключающий сигнал
	private boolean Stpw1 = false; // текущий выходной отключающий сигнал
	
	private double ro() {
		return 1;
	}
	
	public SMNeuron(String name){
		super(name);
	}
	
	public SMNeuron(){
		
	}
	
	public SMNeuron(String name, int sM, int sQ) {
		super(name);
		this.M=sM;
		this.Q=sQ;
	}

	// входящие нейроны
	
	// рассчитать следующую партию
	public void calculate() {
		int hw=0;
		
		Enumeration<SMConnection> elements = inputs.elements();
		while (elements.hasMoreElements()) {
			SMConnection connection = elements.nextElement();
			if(((NeuroInterface)connection.getSource()).getSignal())
				hw++;
		}
		
		/*for (NeuroInterface neuron : inNeurons)
			if(neuron.getSignal()) hw++;*/
		boolean btw = ((double)hw/(double)inputs.size()) >= ro();
		if (btw) N++;
		boolean ltw = N >= M;
		if(btw&ltw) P++;
		boolean gtw = P >= Q;
		//Opt1 = !Stw&(btw&ltw|Ot);
			
		if(Ot == true && Stw == true)
			Ot = false;
		
		Opt1 = btw&ltw|Ot;
		
		
		Zpt1 = btw&ltw&gtw;
		
			
		/*for (NeuroInterface neuron : inNeurons)
			neuron.setSignalZ(Zpt1);*/
		
		Enumeration<SMConnection> elementss = inputs.elements();
		while (elementss.hasMoreElements()) {
			SMConnection connection = elementss.nextElement();
			((NeuroInterface)connection.getSource()).setSignalZ(Zpt1);
		}

	}
	
	// взять значения
	public boolean getSignal() {
		return Ot;
	}

	public boolean getSignalZ() {
		return Zt;
	}
	
	@Override
	public void setSignalZ(boolean zpt1) {
		//System.out.println("dd");
		if(zpt1==true)
			Stpw1 = zpt1;
	}

	@Override
	public void printState() {
		// TODO Auto-generated method stub
		
	}

	/*public void addInput(NeuroInterface neuron) {
		inNeurons.add(neuron);
	}*/

	public void next() {
		Ot = Opt1;
		Zt = Zpt1;
		Stw = Stpw1;
		Stpw1=false;
	}

	/*public List<NeuroInterface> getInnerNeurons() {
		return inNeurons;
	}*/



	public boolean getState() {
		return N >= M;
	}

	public Object getCounterBtw() {
		return N;
	}

	public Object getThresholdBtw() {
		return M;
	}
	
	public Object getThresholdQ() {
		return Q;
	}

	public Object getCounterP() {
		return P;
	}

	public boolean isLeft() {
		return outputs.isEmpty();
	}


	
}
