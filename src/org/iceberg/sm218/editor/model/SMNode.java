package org.iceberg.sm218.editor.model;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.draw2d.geometry.Point;

public class SMNode implements Serializable, INeuroProperty {
	
	private static final long serialVersionUID = 1L;
	protected Point location;

	private static int counter =0;
	
	private String name= ""+counter;
	
	private int code = counter++;

	protected Hashtable<String, SMConnection> inputs = new Hashtable<String, SMConnection>(7);
	protected Vector<SMConnection> outputs = new Vector<SMConnection>(4, 4);
	
	@Override
	public int hashCode() {
		return code;
	}
	
	public SMNode(String name) {
		this.name = name;
	}
	
	public SMNode() {};
	
	public String getName() {
		return name;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setLocation(Point location) {
		this.location = location;
	}
	
	public Point getLocation() {
		return this.location;
	}

	public Vector<SMConnection> getTargetConnections() {
		Enumeration<SMConnection> elements = inputs.elements();
		Vector<SMConnection> v = new Vector<SMConnection>(inputs.size());
		while (elements.hasMoreElements())
			v.addElement(elements.nextElement());
		return v;
	}

	@SuppressWarnings("unchecked")
	public Vector<SMConnection> getSourceConnections() {
		return (Vector<SMConnection>) outputs.clone();
	}

	public void connectOutput(SMConnection wire) {
		outputs.addElement(wire);
	}

	public void connectInput(SMConnection wire) {
		inputs.put(wire.getTargetTerminal() + wire.toString(), wire);
	}

	public void disconnectInput(SMConnection wire) {
		inputs.remove(wire.getTargetTerminal() + wire.toString());
	}

	public void disconnectOutput(SMConnection wire) {
		outputs.removeElement(wire);
	}

}
