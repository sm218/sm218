package org.iceberg.sm218.editor.model;


public class SMTonic extends SMNode implements NeuroInterface {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int current = 0;
	private int[] data ={1,1,1,1,1,1,0,0,0,0,0,0,1,0,1,0,1,0};
	
	boolean Out = false;
	boolean Outn = false;
	
	public SMTonic() {
	}
	
	public void setData(int[] data) {
		this.data = data;
	}
	
	public int[] getData() {
		return data;
	}
	
	public SMTonic(int[] data) {
		this.data = data;
	}
	
	public SMTonic(int[] data, String name) {
		super(name);
		this.data = data;
	}

	public boolean getSignal() {
		return Out;
	}

	public void setSignalZ(boolean zpt1) {
		
	}

	public void calculate() {
		Outn = data[current++] == 0? false: true;
	}

	@Override
	public void printState() {
		// TODO Auto-generated method stub
		
	}

	public void next() {
		Out=Outn;
	}

}
