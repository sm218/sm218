package org.iceberg.sm218.editor.model;

import java.io.Serializable;

public class Triplet implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public SMImage protoImage;
	public SMImage resultImage;
	public SMBinAction action;
	public int counter;
	
	public Triplet(SMImage protoImage, SMBinAction action, SMImage resultImage) {
		this.protoImage = protoImage;
		this.action = action;
		this.resultImage = resultImage;
	}
	
	@Override
	public int hashCode() {
		return protoImage.hashCode() + resultImage.hashCode() + action.hashCode();
	}
	
	@Override
	public boolean equals(Object object) {
		if(object instanceof Triplet) {
			return ((Triplet)object).protoImage.equals(protoImage) &&
				((Triplet)object).action.equals(action) &&
				((Triplet)object).resultImage.equals(resultImage);
		}
		return false;
	}
	
}
