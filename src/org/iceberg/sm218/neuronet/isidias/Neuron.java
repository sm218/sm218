package org.iceberg.sm218.neuronet.isidias;

import java.io.Serializable;

import org.iceberg.sm218.neuronet.isidias.groups.IGroup;
import org.iceberg.sm218.neuronet.isidias.groups.NeuronInputGroup;
import org.iceberg.sm218.neuronet.isidias.groups.NeuronOutputGroup;

public class Neuron implements Serializable {

	private static final long serialVersionUID = 1L;

	private NeuronInputGroup[] in;
	private NeuronOutputGroup[] out;
	
	public Neuron(int inputGroupsCount, int outputGroupsCount) {
        in = new NeuronInputGroup[inputGroupsCount];
        for (int i = 0; i < in.length; i++)
            in[i] = new NeuronInputGroup();

        out = new NeuronOutputGroup[outputGroupsCount];
        for (int i = 0; i < outputGroups.length; i++)
            out[i] = new NeuronOutputGroup();
	}
	
	public Neuron() {
		this(1,1);
	}
	
    public void process()
    {
		//foreach (NeuronInputGroup<T> inputGroup in inputGroups)
		//{
		//    inputGroup.Process();
		//}
    }
    
    public void Remove()
    {
        for(IGroup group : in)
            group.remove();
    
        for(IGroup group : out)
            group.remove();
    }

}
