package org.iceberg.sm218.neuronet.isidias.events;

public class LengthChangedEvent extends NeuroNetEvent {

	public LengthChangedEvent(Object source, Type type) {
		super(source, type);
	}

	private static final long serialVersionUID = 1L;

	private int length;
	
	public int getLength() {
		return this.length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
}
