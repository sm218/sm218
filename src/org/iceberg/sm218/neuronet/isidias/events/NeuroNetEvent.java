package org.iceberg.sm218.neuronet.isidias.events;

import java.util.EventObject;

public class NeuroNetEvent extends EventObject {

	private static final long serialVersionUID = 1L;
	
	public enum Type {
		NNE_REMOVED,
		NNE_NEEDS_TO_BE_REMOVED,
		NNE_LENGTH_CHANGED
	}

	private Type type;
	
	public Type getType() {
		return type;
	}
	
	public NeuroNetEvent(Object source, Type type) {
		super(source);
	}

}
