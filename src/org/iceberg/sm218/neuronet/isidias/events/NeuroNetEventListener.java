package org.iceberg.sm218.neuronet.isidias.events;

import java.util.EventListener;

public interface NeuroNetEventListener extends EventListener {
	
	public void actionPerformed(NeuroNetEvent event);
	
}
