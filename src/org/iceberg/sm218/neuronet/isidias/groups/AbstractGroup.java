package org.iceberg.sm218.neuronet.isidias.groups;

import java.util.List;

import org.iceberg.sm218.neuronet.isidias.events.NeuroNetEvent;
import org.iceberg.sm218.neuronet.isidias.events.NeuroNetEventListener;

public abstract class AbstractGroup implements IGroup {
	
	private int length;
	private String name;
	private String description;
	private List<NeuroNetEventListener> listeners;
	
	public AbstractGroup(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public AbstractGroup(String name) {
		this(name, "");
	}
	
	public AbstractGroup() {
		this("NeuronGroup", "");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int getLength() {
		return length;
	}

	@Override
	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public void remove() {
		NeuroNetEvent removeEvent = new NeuroNetEvent(this, NeuroNetEvent.Type.NNE_REMOVED);
		if(listeners != null && !listeners.isEmpty())
			for(NeuroNetEventListener removeListener : listeners)
				removeListener.actionPerformed(removeEvent);		
	}
}
