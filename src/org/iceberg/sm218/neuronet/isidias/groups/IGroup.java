package org.iceberg.sm218.neuronet.isidias.groups;

public interface IGroup {
	
	public String getName();
	public void setName(String name);
	public String getDescription();
	public void setDescription(String description);
	public int getLength();
	public void setLength(int length);
	
	void remove();
	
}
