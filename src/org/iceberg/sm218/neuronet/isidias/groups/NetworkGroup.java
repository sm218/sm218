package org.iceberg.sm218.neuronet.isidias.groups;

public abstract class NetworkGroup<T> extends AbstractGroup<T> {
	
	private String tag;
	
	public NetworkGroup() {
		super("Array");
	}
	
    public NetworkGroup(String name) {
    	super(name);
    }

    public NetworkGroup(String name, String description) {
    	 super(name, description);
    }
    
    public NetworkGroup(String name, String description, String tag) {
    	super(name, description);
        this.tag = tag;
    }
	
	public String getTag() {
		return this.tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
}
