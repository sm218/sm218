package org.iceberg.sm218.neuronet.isidias.groups;

public abstract class NeuronGroup<T> extends AbstractGroup<T> {

	public NeuronGroup(String name, String description) {
		super(name, description);
	}

	public NeuronGroup(String name) {
		super(name);
	}

	
}
