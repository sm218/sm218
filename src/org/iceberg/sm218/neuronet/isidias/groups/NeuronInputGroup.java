package org.iceberg.sm218.neuronet.isidias.groups;

import java.util.ArrayList;
import java.util.List;

import org.iceberg.sm218.neuronet.isidias.outputs.IOutput;

public class NeuronInputGroup<T> extends NeuronGroup<T> {

    private List<IOutput<T>> connectedOutputs = new ArrayList<IOutput<T>>();
    private List<T> input = new ArrayList<T>();

    public NeuronInputGroup() {
    	super("Input","");
    }

    public NeuronInputGroup(String name) {
    	super(name, "");
    }

    public NeuronInputGroup(String name, String description) {
    	super(name, description);
    }
    
    /*public T get(int index) {
    	return input.size() <= index ? null : input.get(index);
    }*/
 
}
