package org.iceberg.sm218.neuronet.isidias.outputs;

import org.iceberg.sm218.neuronet.isidias.groups.IGroup;

public interface IOutput {
	
	public IGroup getOutputGroup();
	
	public Object getValue();

	public void remove();
	
}
