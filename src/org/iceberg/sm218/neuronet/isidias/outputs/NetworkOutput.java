package org.iceberg.sm218.neuronet.isidias.outputs;

import java.util.List;

import org.iceberg.sm218.neuronet.isidias.events.NeuroNetEvent;
import org.iceberg.sm218.neuronet.isidias.events.NeuroNetEventListener;
import org.iceberg.sm218.neuronet.isidias.groups.IGroup;

public class NetworkOutput<T> implements IOutput {

	private int index;
	private NetworkInputGroup outputGroup;
	private List<NeuroNetEventListener> listeners;
	
    public NetworkOutput(NetworkInputGroup outputGroup, int index) {
        this.outputGroup = outputGroup;
        this.index = index;
    }
	
	public int getIndex() {
		return index;
	}
	
	@Override
	public IGroup getOutputGroup() {
		return (IGroup)outputGroup;
	}

	@Override
	public IGroup getValue() {
		return outputGroup[index];
	}

	@Override
	public void remove() {
		NeuroNetEvent removeEvent = new NeuroNetEvent(this, NeuroNetEvent.Type.NNE_NEEDS_TO_BE_REMOVED);
		if(listeners != null && !listeners.isEmpty())
			for(NeuroNetEventListener removeListener : listeners)
				removeListener.actionPerformed(removeEvent);		
	}

}
